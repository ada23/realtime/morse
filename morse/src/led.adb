package body led is
    use raspio.GPIO ;
    function Create( gpio : raspio.GPIO.Pin_Id_Type ) return RPi_Led_Type is
        result : RPi_Led_Type := ( pin => Raspio.GPIO.Create
                                        (Pin_ID => gpio , 
                                        Mode => Output,
                                        Internal_Resistor => Pull_Down ));
    begin
        return result ;
    end Create;

    overriding
    procedure Set( led : RPi_LED_Type ; state : Boolean ) is
    begin
        if state
        then
            Raspio.GPIO.Turn_On( led.pin );
        else
            Raspio.GPIO.Turn_Off( led.pin );
        end if ;
    end Set ;

    overriding
    procedure About( led : RPi_LED_Type ; S : String ) is
    begin
        null ;
    end About ;
begin
    Raspio.Initialize;
end led ;