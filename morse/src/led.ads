with realtime ;

with Raspio.GPIO;

package led is
    pragma Elaborate_Body;
    type RPi_Led_Type is new realtime.LED_Type with
    record
        pin : Raspio.GPIO.Pin_Type ; 
    end record ;

    function Create( gpio : raspio.GPIO.Pin_Id_Type ) return RPi_Led_Type ;
    
    overriding
    procedure Set( led : RPi_LED_Type ; state : Boolean ) ;
    overriding
    procedure About( led : RPi_LED_Type ; S : String ) ;
end led ;