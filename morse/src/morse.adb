with Ada.Command_Line; use Ada.Command_Line ;
with morsecode.signal ;
with led ;

with Raspio.GPIO ;

procedure morse is
   myled : led.RPi_Led_Type := led.Create( Raspio.GPIO.GPIO_P1_07 );
   msg : String := Argument(1) ;
begin
   loop
      morsecode.signal.Generate( msg , myled );
   end loop ;
end morse;